# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


users = 25.times do | x |
  user = User.create
end

rand(18..22).times do | x |
  Age.create(age: rand(1..121), user_id: rand(1..25) )
end

rand(20..25).times do | x |
  Name.create(first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, user_id: rand(1..25))
end