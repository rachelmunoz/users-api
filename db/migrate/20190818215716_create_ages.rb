class CreateAges < ActiveRecord::Migration[5.0]
  def change
    create_table :ages do |t|
      t.integer :age
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
