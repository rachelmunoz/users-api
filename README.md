# users-api

## Data Models

Since the id columns have a foreign key relationship to each other, the data corresponding to the enpdoints /names and /ages belong to a primary model of User.

Three models: User, Name, Age

Where Name and Age have a foreign key relationship to User 

![models diagram](./models.jpg)

## Technologies Used

Rails 5
SQLite3 
Rspec, Factory Bot, Faker

## Set up

###### Create DB and seed data 
rake:db create

rake:db migrate

rake:db seed

###### Start server
bin/rails s -p 3001

## Endpoints


- [http://localhost:3001/users](http://localhost:3001/users)
returns a list of objects with the fields user_id and age
- [http://localhost:3001/names](http://localhost:3001/names)
returns a list of objects with the fields user_id, first_name, and last_name

## Tests
Happy path tests included for Models and Requests. 

Would also test:

- Controllers

- As more fuctionality is added, such as CRUD operations, more testing would be added to ensure methods working correctly and JSON responses formatted correctly

## Assumptions

- There is no authentication or authorization for accessing data from the endpoints
- Happy path is taken. Responses return 200, no custom response object with proper status code and message set up 
