require 'rails_helper'

RSpec.describe 'Ages API', type: :request do
  let!(:users) { create_list(:user, 10) }
  
  describe 'GET /ages' do
    before { get '/ages' }
     
    it 'returns object of ages' do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end
end
