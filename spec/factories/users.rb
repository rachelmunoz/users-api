FactoryBot.define do
  factory :user do
    after :create do |user|
      create :age, age: rand(1..121), user: user  
      create :name, first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, user: user
    end
  end
end


