FactoryBot.define do
  factory :age do
    age { rand(1..101)}
    user_id { Faker::Number.unique.between(1,10)}
  end
end