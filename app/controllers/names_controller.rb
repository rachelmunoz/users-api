class NamesController < ApplicationController
    def index
      @names = Name.all
      json_response(@names)
    end
end
