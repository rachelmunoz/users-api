class AgesController < ApplicationController
    def index
      @ages = Age.all
      json_response(@ages)
    end
end
