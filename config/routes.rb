Rails.application.routes.draw do
  resources :ages, :names, :only => [:index]
end
